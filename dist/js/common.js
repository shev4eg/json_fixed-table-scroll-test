$(function() {

    $('input[placeholder], textarea[placeholder]').placeholder();

    var table = $('#table');

    $.getJSON('data.json', function(data) {
        var headers = data[0],
            columns = headers.length,
            tableLength = data.length;

        for(var h=0;h<1;h++){
            table.append('<thead><tr></tr></thead>');

            for(var j=0;j<columns;j++){
                table.find('tr').eq(h).append('<th>'+ data[0][j] +'</th>');
            }
        }

        for(var i=1;i<tableLength;i++){
            table.append('<tr></tr>');
            for(var j=0;j<columns;j++){
                table.find('tr').eq(i).append('<td>'+ data[i][j] +'</td>');
            }
        }
    });

    function tableScroll(){
        table.tableHeadFixer({
            head: true,
            foot: false,
            left: 1,
            right: 0,
            'z-index': 0
        });
    }

    setTimeout(function () {
        tableScroll();
    },100)

});
